
var express = require('express');
var http = require('http');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var expressvalidator = require('express-validator');
var expressJwt = require('express-jwt');
var jwt = require('jsonwebtoken');


//mongoDB
var db=require('./model/db');
var passport = require('passport');
//klassen
require('./model');
require('./model/users');
require('./config/passport');
//config





var methodOverride = require('method-override');







//routes
var routes = require('./routes/index');
var fietsTypes = require('./routes/fietsTypes');
var merken = require('./routes/merken');
var users = require('./routes/users');

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
//app.use('/api', expressJwt({secret: 's1e2c3r4e5t6'}));
app.use(bodyParser.json());
app.use(expressvalidator([]));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
//app.use(passport.session());

// override with POST having ?_method=DELETE
app.use(methodOverride('_method'));


app.use(methodOverride(function(req, res){
  if (req.body && typeof req.body === 'object' && '_method' in req.body) {
    // look in urlencoded POST bodies and delete it
    var method = req.body._method
    delete req.body._method
    return method
  }
}));


app.use('/', routes);
//app.use('/users', users);
app.use('/merken',merken);
app.use('/fietsTypes',fietsTypes);
app.use('/users',users);






// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});



module.exports = app;
