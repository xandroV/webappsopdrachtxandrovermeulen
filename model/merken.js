/**
 * Created by Xandro on 15/11/2015.
 */

var mongoose=require('mongoose');

var MerkSchema=new mongoose.Schema({
  naam:String,
 url:String,
    omschrijving:String,
  fietsTypes:[{type:mongoose.Schema.Types.ObjectId,ref:'FietsType'}]
});


MerkSchema.methods.addType=function (fietsType){

  if(this.fietsTypes.indexOf(fietsType._id)==-1){
    this.fietsTypes.push(fietsType);

  }  else{
    console.log("Het opgegeven fietsType staat al in de lijst van types.");
  }
  this.save();

};

MerkSchema.methods.verwijderAlleTypes=function (){

this.fietsTypes=[];
  this.save();
};




mongoose.model('Merk',MerkSchema);


