/**
 * Created by Xandro on 15/11/2015.
 */
var mongoose=require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');


var UserSchema=new mongoose.Schema({

    hash:String,//
    salt:String,//
   username:{type:String,required:true,unique:true}

});

UserSchema.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password,this.salt,1000,64).toString('hex');
};

UserSchema.methods.validPassword = function(password){
    var hash = crypto.pbkdf2Sync(password,this.salt,1000,64).toString('hex');

    return this.hash === hash;
};

UserSchema.methods.generateJWT = function() {

    // set expiration to 60 days
    var today = new Date();
    var exp = new Date(today);
    exp.setDate(today.getDate() + 1000000);

    return jwt.sign({
        _id: this._id,
        username: this.username,
        exp: parseInt(exp.getTime() / 1000),
    }, 's1e2c3r4e5t6');
};

mongoose.model('User',UserSchema);