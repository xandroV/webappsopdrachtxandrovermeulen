var app = angular.module('tweewielers', []);







app.controller('MainCtrl', [
  '$scope','$http','fietstypeService',

    function($scope,$http,fietstypeService){
      $scope.titel="test";



            var o = {
                fietsTypes : [],
                merken:[]
            };

        $http.get('/fietsTypes').success(function(data) {

                    angular.copy(data, o.fietsTypes);
                });



        $http.get('/merken').success(function(data) {

            angular.copy(data, o.merken);
        });

        var arrayOmzetten=function(types){
            var output="";
            for (var i = 0; i < types.length; i++) {
                output+=types[i]+" ";

            }
            return output;
        };




        $scope.fietsen = o.fietsTypes;
        //merk heeft types
        $scope.merken= o.merken;

$scope.fietstypeNamenAanvullen=function(merk,fietsTypes){
   return fietstypeService.fietstypeNamenAanvullen(merk,fietsTypes);

}

    }]);


app.factory('auth', ['$http', '$window',
    function($http, $window) {
        var auth = {};

        auth.saveToken = function(token) {
            $window.localStorage['tweewielers-token'] = token;
        };

        auth.getToken = function() {
            return $window.localStorage['tweewielers-token'];
        }

        auth.isLoggedIn = function() {
            var token = auth.getToken();

            if (token) {
                var payload = JSON.parse($window.atob(token.split('.')[1]));

                return payload.exp > Date.now() / 1000;
            } else {
                return false;
            }
        };

        auth.currentUser = function() {
            if (auth.isLoggedIn()) {
                var token = auth.getToken();
                var payload = JSON.parse($window.atob(token.split('.')[1]));

                return payload.username;
            }
        };

        auth.register = function(user) {
            return $http.post('/register', user).success(function(data) {
                auth.saveToken(data.token);
            });
        };

        auth.logIn = function(user) {
            return $http.post('/login', user).success(function(data) {
                auth.saveToken(data.token);
            });
        };

        auth.logOut = function() {
            $window.localStorage.removeItem('tweewielers-token');
        };

        return auth;
    }]);


app.controller('AuthCtrl', ['$scope', '$state', 'auth',
    function($scope, $state, auth) {
        $scope.user = {};

        $scope.register = function() {
            auth.register($scope.user).error(function(error) {
                $scope.error = error;
            }).then(function() {
                $state.go('home');
            });
        };

        $scope.logIn = function() {
            auth.logIn($scope.user).error(function(error) {
                $scope.error = error;
            }).then(function() {
                $state.go('home');
            });
        };
    }]);



app.directive('winkelOpen',function() {

var d=new Date();
var dag= d.getDay();
    var now = getMinutesNow();
var gesloten=true;



    switch(dag){
        default:
                    if(vergelijkUren('8:00','12:15')||vergelijkUren('13:00','18:30')){
                    var gesloten=false;

                                }break;
        case 6:
            if(vergelijkUren('9:00','12:30')||vergelijkUren('13:30','17:00')){
                var gesloten=false;

            }break;


        case 0:
            break;

    }

    function vergelijkUren(str1,str2){
        var start = getMinutes(str1);
        var end = getMinutes(str2);
        if (start > end) end += getMinutes('24:00');
        if ((now > start) && (now < end)) {

           return true;
        }


    }

    function getMinutes(str) {
        var time = str.split(':');
        return time[0]*60+time[1]*1;
    }
    function getMinutesNow() {
        var timeNow = new Date();
        return timeNow.getHours()*60+timeNow.getMinutes();
    }

    if(gesloten){
    return {
        restrict: 'A',
        replace: true,

        template: '\
          <div>\
            <h3>de winkel is nu gesloten!</h3>\
          </div>\
        '
    }}
    else{
        return {
            restrict: 'A',
            replace: true,

            template: '\
          <div>\
            <h3>de winkel is nu open!</h3>\
          </div>\
        '
        }

    }
});




