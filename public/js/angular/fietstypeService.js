/**
 * Created by Xandro on 4/12/2015.
 */
app.service('fietstypeService',['$http','$window', function($http,$window) {
    var fietstypeService = {};

    fietstypeService.fietstypeNamenAanvullen = function(merk,fietstypes){
        output="";

        for(var i=0;i<merk.fietsTypes.length;i++){
            var id= merk.fietsTypes[i];
            for(var j=0;j<fietstypes.length;j++){
                 if(id===fietstypes[j]._id){
                 output+=fietstypes[j].naam+" ";
                 }
            }


        }
        if (output===""){output="Geen"}
        return output;
    };

    return fietstypeService;
}]);