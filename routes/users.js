/**
 * Created by Xandro on 15/11/2015.
 */
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var jwt = require('express-jwt');


var User = mongoose.model('User');


//GET users/
router.get('/', function (req, res, next) {
    User.find(function (err, users) {
        if (err) { next(err); }
        res.json(users);
    });
});

//GET users/new/
router.get('/new', function (req, res) {
    res.render('users/new', { title: 'Voeg een nieuwe user toe!' });
});


/**
 * @api {get} /api/users/<userID> Specifieke User
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiSuccess {String} username username van de user

 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 * {
 *  @apiSuccess {String} _id ID van de user
 *  @apiSuccess {String} "username":"test"
 *
 * }
 *
 * @apiError UserNietGevonden Het id van de user was niet gevonden
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "UserNietGevonden"
 *     }
 *
 */
//GET users/<userID>
router.get('/:id', function (req, res, next) {
   // req.user
});



/**
 * @api {put} /api/users/<userID>/edit Wijzig User
 * @apiName EditUser
 * @apiGroup User
 *
 * @apiParam {String} username De unieke username van de user
 *

 */
router.put('/:id/edit', function (req, res, next) {

    var username = req.body.username;

    if (!username) {
        return res.status(400).json({ message: 'Please fill out all fields' });
    }
    var user = req.user;
    console.log(user.username);
    user.username = username;

    user.save();

    res.json(user);
});


router.delete(':id/edit', function (req, res, next) {
    req.user.remove();
    res.redirect('/');
})





module.exports = router;
