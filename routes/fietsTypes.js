var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var jwt = require('express-jwt');
//var auth = jwt({secret: 'SECRET', userProperty: 'payload'});


var FietsType = mongoose.model('FietsType');


/**
 * @api {get} /api/fietsTypes Alle fietsTypes
 * @apiName GetFietsTypes
 * @apiGroup FietsType
 */
//GET http://127.0.0.1:3000/fietsTypes
router.get('/',function(req,res, next) {
   FietsType.find(function(err,fietsTypes){
        if(err){return next(err);}
        res.json(fietsTypes);
    });
});






router.get('/overview',function(req,res, next) {


        mongoose.model('FietsType').find({}, function (err, fietsTypes) {
            if (err) {
                return console.error(err);
            } else {

                res.format({

                    html: function(){
                        res.render('fietsTypes/overview', {
                            title: 'Alle fietstypes',
                            "fietsTypes" : fietsTypes
                        });
                    },

                    json: function(){
                        res.json(infophotos);
                    }
                });
            }
        });
    })












//POST http://127.0.0.1:3000/fietsTypes
router.post('/',function(req, res,next) {
    if (!req.body.naam
    ) {
        console.log("niet alle gegevens ingevuld");
        return res.status(400).json({ message: 'Keer terug en vul alle gegevens in!' });
    }
    var fietsType = new FietsType(req.body);
    fietsType.save(function(err,fietsType){
        if(err){return next(err);}
        res.json(fietsType);
    });
});

//GET http://127.0.0.1:3000/fietsTypes/new
router.get('/new', function(req, res) {
    res.render('fietsTypes/new', { title: 'Voeg een nieuw fiets-type toe' });
});

/**
 * @api {get} /api/fietsTypes/<fietsTypeID> Specifiek fietsType
 * @apiName GetFietsType
 * @apiGroup FietsType
 *
 * @apiSuccess {String} _id ID van het type
 * @apiSuccess {Number} naam De naam van het type

 *
 */
//GET http://127.0.0.1:3000/fietsTypes/<fietsTypeID>
router.get('/:id',function(req,res){
    res.json(req.fietsType)
});

//GET http://127.0.0.1:3000/fietsTypes/<fietsTypeID>/edit
router.get('/:id/edit', function (req, res,next) {
    res.render('fietsTypes/edit', {
        title: 'Wijzig Type',
        "fietsType": req.fietsType
    });
});


//PUT http://127.0.0.1:3000/fietsTypes/<fietsTypeID>/edit
router.post('/:id/edit', function (req, res,next) {

    if (!req.body.naam
       ) {
        console.log("niet alle gegevens ingevuld");
        return res.status(400).json({ message: 'Keer terug en vul alle gegevens in!' });
    }
    var fietsType = req.fietsType;
   fietsType.naam=req.body.naam;

   fietsType.save();

    //res.json(fietsType);
    res.redirect('/fietsTypes/overview');

});

//DELETE fietsTypes/<fietsTypeID>/edit
router.delete('/:id/edit', function (req, res, next) {
    req.fietsType.remove();
    res.redirect('/fietsTypes/overview');
});



router.param('id', function(req, res, next, id) {
    var query = FietsType.findById(id);
    query.exec(function(err,fietsType){
        if(err) {return next(err);}
        if (!fietsType) { return next(new Error('kan types niet vinden'));}
        req.fietsType =fietsType;
        return next();
    });
});






module.exports = router;
