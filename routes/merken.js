/**
 * Created by Xandro on 15/11/2015.
 */
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var jwt = require('express-jwt');
//var auth = jwt({secret: 'SECRET', userProperty: 'payload'});

var Merk = mongoose.model('Merk');
var FietsType=mongoose.model('FietsType');

/**
 * @api {get} /api/merken Alle merken
 * @apiName GetMerken
 * @apiGroup Merk
 */
//GET http://127.0.0.1:3000/merken
router.get('/',function(req,res, next) {

    Merk.find(function(err,merken){
        if(err){return next(err);}
        res.json(merken);
    });
});


//POST http://127.0.0.1:3000/merken
router.post('/',function(req, res,next) {
    if (!req.body.naam ||
        !req.body.url||
            !req.body.omschrijving

    ) {
        console.log("niet alle gegevens ingevuld");
        return res.status(400).json({ message: 'Keer terug en vul alle gegevens in!' });
    }
    var merk = new Merk(req.body);
    merk.save(function(err,merk){
        if(err){return next(err);}
       // res.json(merk);
        res.redirect('/merken/overview');
    });
});




router.get('/overview',function(req,res, next) {

    //retrieve all blobs from Monogo
    mongoose.model('Merk').find({}, function (err, merken) {
        if (err) {
            return console.error(err);
        } else {

            res.format({

                html: function(){
                    res.render('merken/overview', {
                        title: 'Alle merken',
                        "merken" : merken
                    });
                },

                json: function(){
                    res.json(infophotos);
                }
            });
        }
    });
});











//GET http://127.0.0.1:3000/merken/new
router.get('/new', function(req, res) {
    res.render('merken/new', { title: 'Add New Merk' });
});

/**
 * @api {get} /api/merken/<merkID> Specifieke Merk
 * @apiName GetMerk
 * @apiGroup Merk
 *
 * @apiSuccess {String} _id ID van het merk
 * @apiSuccess {String} naam De naam van het merk
 * @apiSuccess {String} url De link naar de website van het merk
 *
 */
//GET http://127.0.0.1:3000/merken/<merkID>
router.get('/:id',function(req,res){
  req.merk.populate('fietsTypes',function(err,post){
      if(err){return next(err);}
      res.json(req.merk);
  });

});

//GET http://127.0.0.1:3000/merken/<merkID>/edit
router.get('/:id/edit', function (req, res,next) {
    res.render('merken/edit', {
        title: 'Wijzig Merk',
        "merk": req.merk
    });
});


//PUT http://127.0.0.1:3000/merken/<merkID>/edit
router.post('/:id/edit', function (req, res,next) {


    if (!req.body.naam ||
        !req.body.url
       ) {
        console.log("niet alle gegevens ingevuld");
        return res.status(400).json({ message: 'Keer terug en vul alle gegevens in!' });
    }
    var merk = req.merk;
    merk.naam = req.body.naam;
    merk.url = req.body.url;
    merk.omschrijving=req.body.omschrijving;
    merk.save();
    res.redirect('/merken/overview');
});

//DELETE merken/<merkID>/edit
router.delete('/:id/edit', function (req, res, next) {
    req.merk.remove();
    res.redirect('/merken/overview');
});

/**
 * @api {post} /merken/<fietsTypeID>/types/ Een vriend toevoegen
 * @apiName AddFietsType
 * @apiGroup Merk
 *
 *
 * @apiParam {Number} id Het ID van het fietsType dat je wilt toevoegen
 */
router.get('/:id/typesToevoegen/:type', function (req, res, next) {


    req.merk.addType(req.fietsType);
    res.redirect('/merken/'+req.merk._id+'/typesToevoegen');

});


router.get('/:id/typesToevoegen',function(req,res, next) {
    req.merk.populate('fietsTypes',function(err,merk) {
        console.log("test");
        var huidige = merk.fietsTypes;
        var alleTypes;
        var nieuweTypes = [];

        console.log("huidige: "+huidige);

        //retrieve all blobs from Monogo
        mongoose.model('FietsType').find({}, function (err, fietsTypes) {
            if (err) {
                return console.error(err);
            } else {




                for (i = 0; i < fietsTypes.length; i++) {
                    var nieuw=true;
                for (j = 0; j <huidige.length; j++) {

                    if (fietsTypes[i]._id.equals(huidige[j]._id)) {//!
                      nieuw=false;
                    }
                }
                    if(nieuw){nieuweTypes.push(fietsTypes[i]);
               }

                }




                console.log(nieuweTypes);

                res.format({

                    html: function () {
                        res.render('merken/typesToevoegen', {
                            title: 'Voeg types toe',
                            "merk":merk,
                            "fietsTypes": nieuweTypes
                        });
                    },

                    json: function () {
                        res.json(infophotos);
                    }
                });
            }
        });


    });
});



router.delete('/:id/typesToevoegen/',function(req,res, next) {

req.merk.verwijderAlleTypes();
 res.redirect('/merken/'+req.merk._id+'/typesToevoegen');

});






router.param('id', function(req, res, next, id) {
    var query = Merk.findById(id);
    query.exec(function(err,merk){
        if(err) {return next(err);}
        if (!merk) { return next(new Error('Can\'t find merk'));}
        req.merk = merk;
        return next();
    });
});

router.param('type', function(req, res, next, id) {
    var query = FietsType.findById(id);
    query.exec(function(err,fietsType){
        if(err) {return next(err);}
        if (!fietsType) { return next(new Error('Can\'t find type'));}
        req.fietsType= fietsType;
        console.log("gevonden:"+fietsType.naam);
        return next();
    });
});

module.exports = router;
