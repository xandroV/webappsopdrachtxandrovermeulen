var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var passport = require('passport');
var User = mongoose.model('User');

var jwt = require('express-jwt');
var auth = jwt({secret: 's1e2c3r4e5t6', userProperty: 'payload'});


router.get('/backend',function (req, res, next) {
  res.render('backendIndex', { title: 'Backend' });
});


router.get('/', function (req, res, next) {
    res.render('index', { title: 'Tweewielers' });
});



/**
 * @api {post} /register Registreer een user
 * @apiName Registreer
 * @apiGroup User
 *
 * @apiParam {String} username De naam van de user
 * @apiParam {String} password Het paswoord van de user
 *
 * @apiSuccess {String} token Een Web Token
 */
router.post('/register', function (req, res, next) {

  var username = req.body.username;
  var password = req.body.password;


  if (!username||
      !password
  ) {
    return res.status(400).json({ message: 'Please fill out all fields' });
  }


  var user = new User();

  user.username = username;
  user.paassword =password;

  console.log(user);
  user.save(function (err) {
    if (err) { return next(err); }

    return res.json({ token: user.generateJWT() })
  });
});

/**
 * @api {post} /login Inloggen
 * @apiName Inloggen
 * @apiGroup User
 *
 * @apiParam {String} username De username van de user
 * @apiParam {String} password Het paswoord van de user
 *
 *
 * @apiSuccess {String} token Een Web Token
 */

router.post('/login', function(req, res, next) {
  if(!req.body.username || !req.body.password){
    return res.status(400).json({message: 'Please fill out all fields'});
  }

  passport.authenticate('local',
      function (err, user, info) {
        console.log(user);
        if(err){ return next(err); }

        if(user){
          return res.json({token: user.generateJWT(), id: user._id});
        } else {
          return res.status(401).json(info);
        }
      })(req, res, next);
});



module.exports = router;


